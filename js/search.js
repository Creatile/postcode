$('#findCode').keypress(function (e) {
  if (e.which == 13) {
    $('form#search').submit();
    return false;    //<---- Add this line
  }
});
	$('#findCode').click(function(event){
		$('.alert').hide();
		var result=0;
		event.preventDefault();
		$.ajax({
			type: "GET",
			url: "http://nominatim.openstreetmap.org/search?q=France+"+encodeURIComponent($('#ville').val())+"&format=xml&addressdetails=1&bounded=1&limit=3",
			datatype:"xml",
			success: processXML
		});
		function processXML(xml){
			$(xml).find("searchresults").each(function(){
				var results = "";
				$(xml).find("place").each(function(i,v){
				if(v){
					//console.log(v);
					results += '<p>';
					results += '<span>'+$(v).find('postcode').text()+'</span>';
					results += '<span>, '+$(v).find('town').text()+'</span>';
					results += '<span>, '+$(v).find('state').text()+'</span>';
					results += '</p>';
				}else{
					results += '';
				}
				});
				if(results){
					$('#result').html("Résultat de votre recherche : "+results).fadeIn();
				}else{
					$('#error').html("Aucun résultat pour cette recherche.").fadeIn();
				}
			});
		}
	});
